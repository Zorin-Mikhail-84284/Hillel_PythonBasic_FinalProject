from Classes.ContactsBook import ContactsBook
from Classes.Menu import Menu
from Classes.EMenu import EMenu
from Classes.EEditMenu import EEditMenu
from Classes.ESortMenu import ESortMenu
from Classes.EFindMenu import EFindMenu
from Classes.UsrInput import UsrInput
from Classes.Records import Records
from Classes.Record import Record

from datetime import date


def mnu_show_records(contacts_book: ContactsBook) -> None:
    _view_str = Records.as_view_str("Всі записи", contacts_book.records)
    print(_view_str)


def mnu_add_record(contacts_book: ContactsBook) -> None:
    _width = 15
    _last_name: str = UsrInput.get_str(f"{'Прізвище':{_width}}", 1)
    _first_name: str = UsrInput.get_str(f"{'Ім''я':{_width}}", 1)
    _birth_date: date = UsrInput.get_date(f"{'Дата народження':{_width}}", True)
    _phone_number: str = UsrInput.get_str(f"{'Номер телефону':{_width}}")
    _address: str = UsrInput.get_str(f"{'Адреса':{_width}}")
    contacts_book.add_record(_last_name, _first_name, _birth_date, _phone_number, _address)


def mnu_edit_record(contacts_book: ContactsBook) -> None:
    _rec = contacts_book.get_record_by_id(UsrInput.get_number("Вкажіть id запису, або 0 для відміни редагування",
                                                              contacts_book.get_records_ids() + [0]))
    if _rec is None:
        return

    _str_view = Records.as_view_str("Запис, що редагується", [_rec])
    _width = 15
    _copy = Record(0, _rec.last_name, _rec.first_name, _rec.date_of_birth, _rec.phone_number, _rec.address)
    print(_str_view)
    _mnu_items: dict[int, (str, callable)] = {
        EEditMenu.LAST_NAME: "Прізвище",
        EEditMenu.FIRST_NAME: "Ім'я",
        EEditMenu.DATE_OF_BIRTH: "Дата народження",
        EEditMenu.PHONE: "Телефон",
        EEditMenu.ADDRESS: "Адреса",
        EEditMenu.CANCEL: "Відмінити редагування",
        EEditMenu.SAVE_AND_EXIT: "Зберегти і завершити",
    }
    _mnu = Menu("Меню редагування запису", _mnu_items)
    while True:
        match _mnu.show():
            case EEditMenu.LAST_NAME:
                _last_name: str = UsrInput.get_str(f"{'Прізвище':{_width}}", 1)
                _copy.last_name = _last_name
            case EEditMenu.FIRST_NAME:
                _first_name: str = UsrInput.get_str(f"{'Ім''я':{_width}}", 1)
                _copy.first_name = _first_name
            case EEditMenu.DATE_OF_BIRTH:
                _birth_date: date = UsrInput.get_date(f"{'Дата народження':{_width}}", True)
                _copy.date_of_birth = _birth_date
            case EEditMenu.PHONE:
                _phone_number: str = UsrInput.get_str(f"{'Номер телефону':{_width}}")
                _copy.phone_number = _phone_number
            case EEditMenu.ADDRESS:
                _address: str = UsrInput.get_str(f"{'Адреса':{_width}}")
                _copy.address = _address
            case EEditMenu.CANCEL:
                break
            case EEditMenu.SAVE_AND_EXIT:
                _rec.last_name = _copy.last_name
                _rec.first_name = _copy.first_name
                _rec.date_of_birth = _copy.date_of_birth
                _rec.phone_number = _copy.phone_number
                _rec.address = _copy.address
                break


def mnu_delete_record(contacts_book: ContactsBook):
    _rec_id = UsrInput.get_number("Вкажіть id запису, або 0 для відміни видалення", contacts_book.get_records_ids() + [0])
    _rec = contacts_book.get_record_by_id(_rec_id)
    if _rec is None:
        print("За вказаним id запису не знайдено.")
        return
    _str_view = Records.as_view_str("Запис, що видаляється", [_rec])
    print(_str_view)
    if UsrInput.get_number("Ви дійсно бажаєте видалити його (0 - ні, 1 - так)", [0, 1]):
        if contacts_book.delete_record_by_id(_rec_id):
            print("Запис видалено")
        else:
            print("Помилка при видаленні запису.")
    else:
        print("Видалення запису було відмінено")


def mnu_find_records(contacts_book: ContactsBook):
    _mnu_items: dict[int, (str, callable)] = {
        EFindMenu.BY_DEFAULT: "Показати все",
        EFindMenu.BY_NAME: "За іменем",
        EFindMenu.BY_PHONE: "За телефоном",
    }
    _mnu = Menu("Меню пошуку", _mnu_items)
    _recs = []
    match _mnu.show():
        case ESortMenu.BY_DEFAULT:
            _recs = contacts_book.records
        case ESortMenu.BY_NAME:
            _recs = contacts_book.search_by_name(UsrInput.get_str("Частина імені"))
        case ESortMenu.BY_PHONE:
            _recs = contacts_book.search_by_phone(UsrInput.get_str("Частина телефону"))
    print(Records.as_view_str("Знайдені записи", _recs))


def mnu_sort_records(contacts_book: ContactsBook):
    _mnu_items: dict[int, (str, callable)] = {
        ESortMenu.BY_DEFAULT: "За замовчуванням",
        ESortMenu.BY_NAME: "За іменем",
        ESortMenu.BY_PHONE: "За телефоном",
    }
    _mnu = Menu("Меню сортування записів", _mnu_items)
    match _mnu.show():
        case ESortMenu.BY_DEFAULT:
            contacts_book.sort_by_default()
        case ESortMenu.BY_NAME:
            contacts_book.sort_by_name()
        case ESortMenu.BY_PHONE:
            contacts_book.sort_by_phone()
    mnu_show_records(contacts_book)


def mnu_exit(contacts_book: ContactsBook):
    if UsrInput.get_number("Зберегти контакти перед виходом з програми? (1 - так, 0 - ні)", [0, 1]):
        Records.save_to_file("ContactsBook.csv", contacts_book.records)


def main(contacts_book: ContactsBook):
    _mnu_items: dict[int, (str, callable)] = {
        EMenu.SHOW_RECORDS: "• Показати всі записи",
        EMenu.ADD: "• Додати запис",
        EMenu.EDIT: "• Редагувати запис",
        EMenu.DELETE: "• Видалити",
        EMenu.FIND: "• Пошук",
        EMenu.SORT: "• Сортування",
        EMenu.EXIT: "ВИХІД",
    }
    _mnu = Menu("ОСНОВНЕ МЕНЮ", _mnu_items)

    while True:
        match _mnu.show():
            case EMenu.SHOW_RECORDS:
                mnu_show_records(contacts_book)
            case EMenu.ADD:
                mnu_add_record(contacts_book)
            case EMenu.EDIT:
                mnu_edit_record(contacts_book)
            case EMenu.DELETE:
                mnu_delete_record(contacts_book)
            case EMenu.FIND:
                mnu_find_records(contacts_book)
            case EMenu.SORT:
                mnu_sort_records(contacts_book)
            case EMenu.EXIT:
                mnu_exit(contacts_book)
                break


def load_or_default(file_path: str) -> list[Record]:
    _records = Records.load(file_path)
    if len(_records) == 0:
        _records = Records.by_default()
        print(f"Робота із записами по замовчуванню.")
    else:
        print(f"Завантажено записів: {len(_records)} шт.")
    return _records


if __name__ == "__main__":
    main(ContactsBook(load_or_default("ContactsBook.csv")))
