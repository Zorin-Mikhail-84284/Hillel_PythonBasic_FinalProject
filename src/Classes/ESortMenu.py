from enum import IntEnum


class ESortMenu(IntEnum):
    BY_DEFAULT = 0,
    BY_NAME = 1,
    BY_PHONE = 2,
