from enum import IntEnum


class EFindMenu(IntEnum):
    BY_DEFAULT = 0,
    BY_NAME = 1,
    BY_PHONE = 2,