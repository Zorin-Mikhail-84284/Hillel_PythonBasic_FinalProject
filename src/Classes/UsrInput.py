from datetime import date, datetime

class UsrInput:
    @staticmethod
    def get_number(input_str: str, int_constraints: list[int] = None) -> int:
        """Отримати введене з консолі користувачем число
        :param input_str: Текст запрошення вводу даних для користувача
        :param int_constraints: [Опціонально] Список можливих чисел, що може ввести користувач
        :return: Число, що було отримано від користувача
        """
        _user_input = None
        while _user_input is None:
            try:
                _user_input = int(input(f"{input_str} > "))
                if int_constraints is not None and _user_input not in int_constraints:
                    print(f"\33[41mПОМИЛКА ВВОДУ ДАННИХ.\33[0m оберіть з поміж \33[33mДОСТУПНИХ\33[0m числових значень!")
                    _user_input = None
            except ValueError:
                print(f"\33[41mПОМИЛКА ВВВОДУ ДАННИХ.\33[0m Оберіть \33[33mЧИСЛОВЕ\33[0m значення!")
        return _user_input

    @staticmethod
    def get_str(input_str: str, len_constraint: int = 0) -> str:
        """Отримати введену з консолі користувачем стрічку
        :param input_str: Текст запрошення вводу даних для користувача
        :param len_constraint: [Опціонально] Мінімальна довжина рядка, який повинен ввести користувач
        :return: Рядок, що був отриманий від користувача
        """
        _user_input: str | None = None
        while _user_input is None:
            _user_input = input(f"{input_str} > ")
            if len_constraint is not None and _user_input is not None and len(_user_input) < len_constraint:
                print(f"\33[41mПОМИЛКА ВВОДУ ДАННИХ.\33[0m Ведіть рядок не коротше ніж {len_constraint} символи")
                _user_input = None
        return _user_input

    @staticmethod
    def get_date(input_str: str, can_be_none: bool = False) -> date | None:
        """Отримати введену з консолі користувачем дату
        :param input_str: Текст запрошення вводу даних для користувача
        :param can_be_none: [Опціонально] чи може бути значення отримане від користувача None
        :return: Дата, що була отримана від користувача
        """
        _result: date | None = None
        while _result is None:
            try:
                _usr_input = input(f"{input_str} > ")
                if _usr_input is None or len(_usr_input) == 0:
                    if can_be_none:
                        return None
                    else:
                        raise ValueError("Date is None")

                _temp_res: datetime = datetime.strptime(_usr_input, '%Y-%m-%d')
                if _temp_res is None:
                    print(f"\33[41mПОМИЛКА ВВОДУ ДАННИХ.\33[0m значення обов'язкове!")
                    _result = None
                else:
                    _result = date(_temp_res.year, _temp_res.month, _temp_res.day)
            except ValueError:
                print(f"\33[41mПОМИЛКА ВВВОДУ ДАННИХ.\33[0m Введіть \33[33mДАТУ\33[0m в форматі yyyy-MM-dd")
                _result = None
        return _result


if __name__ == '__main__':
    items = []
    print(items.count())