from Classes.Record import Record
from Classes.StrHelper import StrHelper
from datetime import date


class ContactsBook:
    def __init__(self, records: list[Record]):
        self.__records: dict[int, Record] = {_item.idx: _item for _item in records}

    @property
    def records(self) -> list[Record]:
        return list(self.__records.values())

    def get_records_ids(self) -> list[int]:
        return list(self.__records.keys())

    def add_record(self, last_name: str, first_name: str, birth_date: date, phone: str, address: str) -> None:
        _ids = self.get_records_ids()
        _new_id = (max(_ids) if _ids is not None and len(_ids) > 0 else 0) + 1
        _rec = Record(_new_id, last_name, first_name, birth_date, phone, address)
        self.__records[_new_id] = _rec

    def get_record_by_id(self, idx: int) -> Record | None:
        try:
            _rec = self.__records[idx]
        except KeyError as ex:
            return None
        return _rec

    def delete_record_by_id(self, idx: int) -> Record | None:
        _deletable_rec = self.get_record_by_id(idx)
        if _deletable_rec:
            self.__records.pop(idx)
        return _deletable_rec

    def sort_by_default(self) -> None:
        _sorted = sorted(self.__records.values(), key=lambda x: x.idx)
        self.__records = {_item.idx: _item for _item in _sorted}

    def sort_by_name(self) -> None:
        _sorted = sorted(self.__records.values(), key=lambda x: x.first_name.lower())
        self.__records = {_item.idx: _item for _item in _sorted}

    def sort_by_phone(self) -> None:
        _sorted = sorted(self.__records.values(), key=lambda x: StrHelper.get_only_digits(x.phone_number))
        self.__records = {_item.idx: _item for _item in _sorted}

    def search_by_name(self, value: str) -> list[Record]:
        _founded_recs = [_rec for _rec in self.records if StrHelper.find(_rec.first_name, value)]
        return _founded_recs

    def search_by_phone(self, value: str) -> list[Record]:
        _founded_recs = [_rec for _rec in self.records if StrHelper.find(StrHelper.get_only_digits(_rec.phone_number), value)]
        return _founded_recs





