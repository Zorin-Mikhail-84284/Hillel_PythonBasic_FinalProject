from datetime import date, datetime
from Classes.StrHelper import AsStr


class Record:
    def __init__(self, idx: int, last_name: str, first_name: str, date_of_birth: date, phone_number: str, address: str):
        self.__idx = idx
        self.__last_name = last_name
        self.__first_name = first_name
        self.__date_of_birth = date_of_birth
        self.__phone_number = phone_number
        self.__address = address
        self.__modified_at = datetime.now()

    @property
    def idx(self):
        return self.__idx

    @property
    def last_name(self):
        return self.__last_name

    @last_name.setter
    def last_name(self, value: str):
        self.__last_name = value
        self.__modified_at = datetime.now()

    @property
    def first_name(self):
        return self.__first_name

    @first_name.setter
    def first_name(self, value: str):
        self.__first_name = value
        self.__modified_at = datetime.now()

    @property
    def date_of_birth(self):
        return self.__date_of_birth

    @date_of_birth.setter
    def date_of_birth(self, value: date):
        self.__date_of_birth = value
        self.__modified_at = datetime.now()

    @property
    def phone_number(self):
        return self.__phone_number

    @phone_number.setter
    def phone_number(self, value: str):
        self.__phone_number = value
        self.__modified_at = datetime.now()

    @property
    def address(self):
        return self.__address

    @address.setter
    def address(self, value: str):
        self.__address = value
        self.__modified_at = datetime.now()

    @property
    def modified_at(self):
        return self.__modified_at

    def __str__(self):
        return f"{self.idx:>5} │ {AsStr.date_time(self.modified_at)} │ {self.last_name:15} │ {self.first_name:15} │ " \
               f"{AsStr.date(self.date_of_birth)} │ {self.phone_number:>20} │ {self.address}"

    @staticmethod
    def get_csv_fieldnames() -> list[str]:
        _fieldnames = ['idx', 'modified_at', 'last_name', 'first_name', 'date_of_birth', 'phone_number', 'address']
        return _fieldnames

    def as_csv_row(self) -> dict[str, str | int | date]:
        _csv_row = {'idx': self.idx, 'modified_at': f'{self.modified_at:%Y-%m-%d %H-%M-%S}', 'last_name': self.last_name,
                    'first_name': self.first_name, 'date_of_birth': self.date_of_birth,
                    'phone_number': self.phone_number, 'address': self.address}
        return _csv_row

    @classmethod
    def from_csv_row(cls, csv_row: dict[str, str]):
        _obj = cls(int(csv_row['idx']), csv_row['last_name'], csv_row['first_name'],
                   datetime.strptime(csv_row['date_of_birth'], '%Y-%m-%d').date(),
                   csv_row['phone_number'], csv_row['address'])
        _obj.__modified_at = datetime.strptime(csv_row['modified_at'], '%Y-%m-%d %H-%M-%S')
        return _obj
