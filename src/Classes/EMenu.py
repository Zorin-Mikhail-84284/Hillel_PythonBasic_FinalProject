from enum import IntEnum


class EMenu(IntEnum):
    EXIT = 0,
    SHOW_RECORDS = 1,
    ADD = 2,
    EDIT = 3,
    DELETE = 4,
    FIND = 5,
    SORT = 6,
