from Classes.UsrInput import UsrInput
from Classes.EMenu import EMenu
from typing import Callable


class Menu:
    def __init__(self, menu_header: str, menu_items: dict[int, str]):
        self.__menu_header = f" {menu_header} "
        self.__menu_items = menu_items

    def as_view_str(self):
        def item_as_str(mnu_key: int, mnu_value: str):
            return f" {mnu_key:3}  {mnu_value} "
        width = max([len(item_as_str(_key, _value)) for _key, _value in self.__menu_items.items()] + [len(self.__menu_header)])
        _str = f"\n╭{self.__menu_header:─^{width}}╮"
        for _key, _value in self.__menu_items.items():
            _str += f"\n│{item_as_str(_key, _value):{width}}│"
        _str += f"\n╰{'':─^{width}}╯"
        _str += f"\nЗробіть ваш вибір"
        return _str

    def show(self) -> int:
        _menu = self.as_view_str()
        _menu_key = UsrInput.get_number(_menu, list(self.__menu_items.keys()))
        return _menu_key
