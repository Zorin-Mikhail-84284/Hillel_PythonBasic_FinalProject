import csv
import os
from datetime import date, datetime
from Classes.Record import Record
from Classes.StrHelper import StrHelper
from Classes.StrHelper import AsStr
from Classes.StrHelper import ColumnInfo


class Records:
    @staticmethod
    def by_default() -> list[Record]:
        _recs = [
            Record( 1, "Зорін", "Михайло", date(1984, 10, 10), None, "м. Вінниця"),
            Record( 2, "Ковальчук", "Амвросій", date(1991, 12, 13), None, "м. Вінниця, вул. Польова 98 / 1"),
            Record( 3, "Біловус", "Герман", date(1984, 10, 30), "+38 (097) 654 3210", "м. Вінниця, вул. Лугова 76 / 2"),
            Record( 4, "Олійник", "Алла", date(1978, 11, 15), "+38 (097) 543 2106", "м. Вінниця, вул. Квіткова 54 / 3"),
            Record( 5, "Шевченко", "Гаврило", date(2001, 7, 18), "+38 (097) 432 1056", "м. Вінниця, вул. Спортивна 32 / 4"),
            Record( 6, "Поліщук", "Карпо", date(1978, 4, 5), "+38 (097) 321 0654", "м. Вінниця, вул. Миру 1 / 5"),
            Record( 7, "Марченко", "Олег", date(1992, 6, 11), "+38 (097) 210 6543", "м. Вінниця, вул. Шкільна 12 / 39"),
            Record( 8, "Василенко", "Мар'яна", date(1983, 5, 23), "+38 (097) 106 5432", "м. Вінниця, вул. Інститутська 43 / 22"),
            Record( 9, "Панченко", "Одарка", date(1989, 3, 18), "+38 (097) 065 4321", "м. Вінниця, вул. Абрикосова 14 / 27"),
            Record(10, "Литвиненко", "Кузьма", date(1999, 11, 11), "+38 (096) 754 3210", "м. Вінниця, вул. Ромашкова 22 / 22"),
            Record(11, "Приходько", "Тимур", date(1987, 2, 26), "+38 (096) 543 2107", "м. Вінниця, вул. Фіалкова 34 / 64"),
        ]
        return _recs

    @staticmethod
    def load(file_path: str) -> list[Record]:
        _records: list[Record] = []
        if not os.path.isfile(file_path):
            return _records
        with open(file_path, 'rt', encoding='utf-8', newline='') as csvfile:
            csv_reader = csv.DictReader(csvfile, delimiter="│")
            for _row in csv_reader:
                _record = Record.from_csv_row(_row)
                _records.append(_record)
        return _records

    @staticmethod
    def save_to_file(file_path: str, records: list[Record]) -> None:
        with open(file_path, "wt", encoding='utf-8', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=Record.get_csv_fieldnames(),
                                    quoting=csv.QUOTE_MINIMAL, delimiter='│')
            writer.writeheader()
            for _rec in records:
                writer.writerow(_rec.as_csv_row())

    @staticmethod
    def as_view_str(title: str, records: list[Record]) -> str:
        _str_table = StrHelper.as_table(title, records, [
            ColumnInfo('>id', lambda e: str(e.idx)),
            ColumnInfo('Дата модифікації', lambda e: AsStr.date_time(e.modified_at)),
            ColumnInfo('Прізвище', lambda e: e.last_name),
            ColumnInfo('Ім''я', lambda e: e.first_name),
            ColumnInfo('^Дата народження', lambda e: AsStr.date(e.date_of_birth)),
            ColumnInfo('Телефон', lambda e: e.phone_number),
            ColumnInfo('Адреса', lambda e: e.address),
        ])
        return _str_table
