from enum import IntEnum


class EEditMenu(IntEnum):
    CANCEL = 0,
    SAVE_AND_EXIT = 1,
    LAST_NAME = 2,
    FIRST_NAME = 3,
    DATE_OF_BIRTH = 4,
    PHONE = 5,
    ADDRESS = 6,
