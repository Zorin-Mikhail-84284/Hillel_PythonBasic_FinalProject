# Фінальне завдання • Программа «Записна книга»

> Виконано 21 із 21 (100.00 %)

- [x] Необхідно написати програму "Записна книга"
  - [x] Кожен запис повинен містити поля:
    - [x] Прізвище*
    - [x] Ім'я*
    - [x] Номер телефону
    - [x] Адреса
    - [x] Дата народження
  - [x]  **_Поля помічені `*` обов'язкові для заповнення_**
  - [x] Робота із записною книгою повинна відбуватись через набір текстових, екранних меню. Вибір необхідного пункту меня відбувається з клавіатури шляхом вводу номеру пункту меню.
  - [x] **_В програмі повинен бути реалізований наступний функціонал:_**
    - [x]  Додавання нового запису
    - [x] Видалення запису
    - [x] Редагування запису
    - [x] Пошук за полем `Ім'я`
    - [x] Пошук за полем `Номер телефону`
    - [x] Як додаткова умова, не обов'язкова, доповнити можливість пошуку за маскою. Наприклад, пошук записів за полем `Прізвище` з додаванням маски: `А*`, повинен вернути всі записи, у яких прізвище починається на літеру `А`.
    - [x] Пошук повинен бути не чутливим до регістру
    - [x] Сортування за полем `Ім'я`
    - [x] Сортування за полем `Прізвище`
    - [x] При виході з програми зберігати всі записи в файл
    - [x] Під час запуску програми перевіряти, якщо існує файл із даними, раніше збережений, то завантажити його в програму та повідомити про кількість завантажених записів.

**_Приклад меню:_**
1. Додати запис
2. Видалити запис
3. Змінити запис
4. Пошук
5. Сортування
6. Вихід

Зробіть ваш вибір > _

